
if exists (select * from sysobjects where id = object_id(N'dbo.FixSpaces'))
	drop procedure dbGameServer.FixSpaces
go

create procedure dbo.FixSpaces
as
begin
	update dbo.[C]
	set Cname = ltrim (Cname)
	update dbo.[A]
	set Aname = ltrim (Aname)
	update dbo.[S]
	set Sname = ltrim (Sname)
end
go

update S set Sname = '   a' where Sno = 1
go

exec dbo.FixSpaces
go

select Sname from S
go