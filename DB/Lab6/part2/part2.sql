if exists (select * from sysobjects where id = object_id(N'GetDoubleFact'))
	drop procedure dbo.GetDoubleFact
go

create procedure dbo.GetDoubleFact
(
	@val int,
	@res float output
)
as
begin
	set @res = 1.0
	declare @start int
	if ((@val % 2) = 0)
		set @start = 2
	else
		set @start = 1;
	with Numbers (num) as
	(
		select @start
		union all
		select num + 2
		from Numbers
		where num < @val
	)
	select @res = @res * num
	from Numbers
end
go

declare @val int
declare @res float

set @val = 4
exec dbo.GetDoubleFact @val, @res output
select @res

set @val = 5
exec dbo.GetDoubleFact @val, @res output
select @res

go