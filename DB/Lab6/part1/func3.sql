if exists (select * from sysobjects where id = object_id(N'dbo.GetDigitsMult'))
	drop function dbo.GetDigitsMult
go

create function GetDigitsMult(@val int)
returns @Digits table (digit int null)
as
begin
	while (@val <> 0)
	begin
		insert into @Digits values (@val % 10)
		set @val /= 10
	end
	return
end
go

select * from dbo.GetDigitsMult(123)
select * from dbo.GetDigitsMult(2147483647)
select * from dbo.GetDigitsMult(null)
go