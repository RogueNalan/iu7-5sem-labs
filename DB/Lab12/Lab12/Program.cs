﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace Lab12
{
    [DataContract]
    public class MyTime
    {
        [DataMember] // в сериализованном виде поле будет называться "time"
        public double unixtime = 1;

        [DataMember]
        public int value = 2;
    }

    public class MyTime2
    {
        [DataMember]
        public int value = 3;

        [DataMember]
        public double unixtime = 4;

    }

    class Program
    {
        static void Main(string[] args)
        {
            var ser = new DataContractJsonSerializer(typeof(MyTime));
            var time = new MyTime();
            var stream = new MemoryStream();

            ser.WriteObject(stream, time);  // в поток записываем сериализованный объект
            stream.Position = 0;            // устанавливаем позицию внутри потока на 0 для чтения

            MyTime2 time2 = (MyTime2)new DataContractJsonSerializer(typeof (MyTime2)).ReadObject(stream);

            Console.WriteLine(time2.unixtime); // будет выведен 0
            Console.ReadLine();
        }
    }
}
