﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Xml;

namespace Lab4
{
    internal class XmlProcessor
    {
        public void Open()
        {
            Console.WriteLine("Введите имя файла");
            string filename = Console.ReadLine();

            var filestream = new FileStream(filename, FileMode.Open);
            XmlValidatingReader reader = new XmlValidatingReader(filestream, XmlNodeType.Document, null);
            document.Load(reader);
            filestream.Close();

            Console.WriteLine("Файл был загружен!");
        }

        public void Find()
        {
            Console.WriteLine("\ta. GetElementByTagName"); // +
            Console.WriteLine("\tb. GetElementById"); // +
            Console.WriteLine("\tc. SelectNodes"); // +
            Console.WriteLine("\td. SelectSingleNode"); // +

            Action[] funcs = new Action[]
            {
                FindA, FindB, FindC, FindD
            };

            char c = Console.ReadKey().KeyChar;
            Console.Write('\b');

            funcs[c - 'a']();
        }

        public void Access()
        {
            Console.WriteLine("\ta. XmlElement"); // +
            Console.WriteLine("\tb. XmlText"); // -
            Console.WriteLine("\tc. XmlComment"); // -
            Console.WriteLine("\td. XmlProcesingInstruction"); //-
            Console.WriteLine("\te. Атрибуты узлов"); // +

            Action[] funcs = new Action[]
            {
                AccessA, AccessB, AccessC, AccessD, AccessE
            };

            char c = Console.ReadKey().KeyChar;
            Console.Write('\b');

            funcs[c - 'a']();
        }

        public void Change()
        {
            Console.WriteLine("\ta. Удалить");  // +
            Console.WriteLine("\tb. Изменить"); // +
            Console.WriteLine("\tc. Создать");  // +
            Console.WriteLine("\td. Вставить"); // +
            Console.WriteLine("\te. Добавить"); // +

            Action[] funcs = new Action[]
            {
                ChangeA, ChangeB, ChangeC, ChangeD, ChangeE
            };

            char c = Console.ReadKey().KeyChar;
            Console.Write('\b');

            funcs[c - 'a']();
        }

        private void FindA()
        {
            Console.WriteLine("Введите название тега");
            string name = Console.ReadLine();

            PrintAllNodes(document.GetElementsByTagName(name));
        }

        private void FindB()
        {
            Console.WriteLine("Введите ID");
            string id = Console.ReadLine();

            var node = document.GetElementById(id);
            if (node != null)
                Console.WriteLine(node.ChildNodes[0].ChildNodes[0].Value);
            else
                Console.WriteLine("Найти элемент не удалось!");
        }

        private void FindC()
        {
            Console.WriteLine("Введите выражение XPath");
            string xpath = Console.ReadLine();

            var nodes = document.SelectNodes(xpath);
            foreach (XmlNode node in nodes)
                Console.WriteLine(node.ChildNodes[0].Value);
        }

        private void FindD()
        {
            Console.WriteLine("Введите выражение XPath");
            string xpath = Console.ReadLine();

            var node = document.SelectSingleNode(xpath);
            Console.WriteLine(node.ChildNodes[0].Value);
        }

        private void AccessA()
        {
            Console.WriteLine("Введите ID");
            int id = Convert.ToInt32(Console.ReadLine());

            XmlNodeList nodes = document.DocumentElement.ChildNodes;
            foreach (XmlNode node in nodes)
                Console.WriteLine(node.ChildNodes[id].InnerText);
        }

        private void AccessB()
        {
            Console.WriteLine(document.GetElementsByTagName("Aname")[0].ChildNodes[0].Value);
        }

        private void AccessC()
        {
            Console.WriteLine(document.GetElementsByTagName("Aname")[1].ChildNodes[0].Value);
        }

        private void AccessD()
        {
            XmlProcessingInstruction myPI = (XmlProcessingInstruction)
                document.DocumentElement.ChildNodes[0];
            Console.Write("Name: " + myPI.Name + "\r\n");
            Console.Write("Data: " + myPI.Data + "\r\n");
        }

        private void AccessE()
        {
            Action<XmlNode> PrintNodeAttribute = (node) =>
                {
                    var col = node.Attributes;
                    foreach (XmlAttribute attr in col)
                        Console.WriteLine("Name: " + attr.Name + " = " + attr.Value);
                };

            var list = document.DocumentElement.ChildNodes;
            for (int i = 0; i < list.Count; ++i)
                PrintNodeAttribute(list[i]);
        }

        private void ChangeA()
        {
            Console.WriteLine("Введите название тега:");
            string tag = Console.ReadLine();

            XmlElement elem = (XmlElement) document.GetElementsByTagName(tag)[0];
            document.DocumentElement.ChildNodes[0].RemoveChild(elem);
            document.Save("remove.xml");
        }

        private void ChangeB()
        {
            Console.WriteLine("Введите выражение XPath");
            string xpath = Console.ReadLine();

            var list = document.SelectNodes(xpath);
            foreach (XmlNode node in list)
                node.Value = node.Value + "1";
            document.Save("change.xml");
        }

        private void ChangeC()
        {
            XmlElement tester = document.CreateElement("tester");
            XmlText value = document.CreateTextNode("false");
            tester.AppendChild(value);

            var list = document.DocumentElement.ChildNodes;
            foreach (XmlNode node in list)
        
                node.AppendChild(tester);

            document.Save("add.xml");
        }

        private void ChangeD()
        {
            Console.WriteLine("Введите название тега");
            string tag = Console.ReadLine();

            XmlElement tester = document.CreateElement("tester");
            XmlText value = document.CreateTextNode("false");
            tester.AppendChild(value);

            var nodeList = document.DocumentElement.ChildNodes;
            var list = document.GetElementsByTagName(tag);
            for (int i = 0; i < nodeList.Count; ++i)
                nodeList[i].InsertAfter(tester, list[i]);

            document.Save("insert.xml");
        }

        private void ChangeE()
        {
            document.DocumentElement.SetAttribute("modified", "true");

            document.Save("attrib.xml");
        }

        private static void PrintAllNodes (XmlNodeList nodes)
        {
            foreach (XmlNode node in nodes)
                PrintNode(node);
        }

        private static void PrintNode (XmlNode node)
        {
            Console.WriteLine(node.ChildNodes[0].Value);
        }

        private XmlDocument document = new XmlDocument();
    }
}
