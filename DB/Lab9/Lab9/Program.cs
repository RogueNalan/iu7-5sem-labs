﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Xml.Linq;

namespace Lab9
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Part1:");
            Part1();
            Console.WriteLine();
            Console.WriteLine("Part2:");
            Part2();
            Console.WriteLine();
            Console.WriteLine("Part3:");
            Part3();
            Console.ReadLine();
        }

        static void Part1()
        {
            var list1 = LinqToObjects.Generate(10);
            var list2 = LinqToObjects.Generate(10);

            list1[0].Clvl = 50; list1[0].Cmoney = 1001;
            list1[1].Clvl = 50;
            list2[5].Clvl = 50;

            Console.WriteLine("Query1:");
            foreach (var x in LinqToObjects.Query1(list1))
                Console.WriteLine(x);

            Console.WriteLine("Query2:");
            Console.WriteLine(LinqToObjects.Query2(list1));

            Console.WriteLine("Query3: ");
            foreach (var x in LinqToObjects.Query3(list1, list2))
                Console.WriteLine(x);

            Console.WriteLine("Query4: ");
            foreach (var x in LinqToObjects.Query4(list1))
            {
                Console.WriteLine("Key: " + x.Key.ToString());
                foreach (var y in x)
                    Console.WriteLine ("Cname: " + y.Cname);
            }

            Console.WriteLine("Query 5:");
            LinqToObjects.Query5(list1);
        }

        static void Part2()
        {
            var ltx = new LinqToXml("c.xml");

            Console.WriteLine(ltx.Query1());

            ltx.Query2();
            ltx.Query3();
        }

        static void Part3()
        {
            var lts = new LinqToSql();

            var res = lts.Query1();

            Console.WriteLine("Query 1:");
            foreach (string s in res)
                Console.WriteLine(s);

            Console.WriteLine("Query 2:");
            Console.WriteLine(lts.Query2());

            Console.WriteLine("Query 3:");
            lts.Query3();
            Console.WriteLine("Query 4:");
            lts.Query4();
            Console.WriteLine("Query 5:");
            lts.Query5();
            Console.WriteLine("Query 6:");
            Console.WriteLine(lts.Query6(4));
        }
    }
}
