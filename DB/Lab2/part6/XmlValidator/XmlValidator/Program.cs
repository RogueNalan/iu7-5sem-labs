﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Xml;
using System.Xml.Schema;


namespace XmlValidator
{
    class Program
    {
        static void Main(string[] args)
        {
            var settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            settings.Schemas.Add("", "a_vs.xsd");
            settings.ValidationEventHandler += ((sender, e) => HandleValidationFail(sender as XmlReader));

            var reader = XmlReader.Create("a.xml", settings);

            try
            {
                while (reader.Read() == true) {}
            }
            catch (XmlException)
            {
                HandleValidationFail(reader);
            }

            Console.WriteLine("File was succesfully validated!\n");
            Console.ReadLine();
        }

        static void HandleValidationFail(XmlReader reader)
        {
            Console.WriteLine("There were an validating error!");
            reader.Close();
            Console.ReadLine();
            Environment.Exit(1);
        }
    }
}
;