declare @cxml xml
select @cxml = x.a from openrowset(bulk 'E:\iu7-5sem-labs\DB\lab2\part2\c.xml', single_blob) as x(a)

declare @axml xml
select @axml = x.a from openrowset(bulk 'E:\iu7-5sem-labs\DB\lab2\part2\a.xml', single_blob) as x(a)

declare @sxml xml
select @sxml = x.a from openrowset(bulk 'E:\iu7-5sem-labs\DB\lab2\part2\s.xml', single_blob) as x(a)

declare @casxml xml
select @casxml = x.a from openrowset(bulk 'E:\iu7-5sem-labs\DB\lab2\part2\cas.xml', single_blob) as x(a)

declare @cdoc int, @adoc int, @sdoc int, @casdoc int

exec sp_xml_preparedocument @cdoc output, @cxml;
exec sp_xml_preparedocument @adoc output, @axml;
exec sp_xml_preparedocument @sdoc output, @sxml;
exec sp_xml_preparedocument @casdoc output, @casxml; 

use [dbGameServer]

set identity_insert C on
insert into C (Cno, Cname, Clvl, Cmoney)
select * from openxml (@cdoc, '/ROOT/C') with
(
	Cno int 'Cno',
	Cname char(11) 'Cname',
	Clvl smallint 'Clvl',
	Cmoney int 'Cmoney'
);
set identity_insert C off

set identity_insert A on
insert into A (Ano, Aname, Aphash, AcrDate)
select * from openxml (@adoc, '/ROOT/A') with
(
	Ano int 'Ano',
	Aname char(16) 'Aname',
	Aphash int 'Aphash',
	AcrDate date 'AcrDate'
);
set identity_insert A off

set identity_insert S on
insert into S (Sno, Sname, Smaxplayers)
select * from openxml (@sdoc, '/ROOT/S') with
(
	Sno int 'Sno',
	Sname char(11) 'Sname',
	Smaxplayers int 'Smaxplayers'
);
set identity_insert S off

insert into CAS (Cno, Ano, Sno)
select * from openxml (@casdoc, '/ROOT/CAS') with
(
	Cno int 'Cno',
	Ano int 'Ano',
	Sno int 'Sno'
);

exec sp_xml_removedocument @cdoc;
exec sp_xml_removedocument @adoc;
exec sp_xml_removedocument @sdoc;
exec sp_xml_removedocument @casdoc; 

use [master]
GO

