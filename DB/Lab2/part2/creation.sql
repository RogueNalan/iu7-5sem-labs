USE [master]
GO

IF EXISTS (SELECT name FROM sys.databases WHERE name = N'dbGameServer')
	DROP DATABASE [dbGameServer]
GO

CREATE DATABASE [dbGameServer]
GO

USE [dbGameServer]
GO

CREATE TABLE [C]
(
	[Cno][int] IDENTITY(1,1) NOT NULL,
	[Cname][char](11) NOT NULL,
	[Clvl][smallint] NOT NULL,
	[Cmoney][int] NULL
)
GO

CREATE TABLE [A]
(
	[Ano][int] IDENTITY(1,1) NOT NULL,
	[Aname][char](16) NOT NULL,
	[Aphash][int] NOT NULL,
	[AcrDate][date] NOT NULL
)
GO

CREATE TABLE [S]
(
	[Sno][int] IDENTITY(1,1) NOT NULL,
	[Sname][char](11) NOT NULL,
	[Smaxplayers][int] NOT NULL
)
GO

CREATE TABLE [CAS]
(
	[Cno][int] NOT NULL,
	[Ano][int] NOT NULL,
	[Sno][int] NOT NULL
)
GO

set identity_insert C	off
set identity_insert A	off
set identity_insert S	off
go

USE [master]
GO