<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:template match="/">
  <html>		
    <body>
      <h1>My Guitars</h1>
      <hr />
      <table width="50%" border="1">
        <tr bgcolor="red">
          <td><b>Nickname</b></td>
          <td><b>Level</b></td>
          <td><b>Money</b></td>
        </tr>
        <xsl:for-each select="ROOT/C">
        <tr>
          <td><xsl:value-of select="Cname" /></td>
          <td><xsl:value-of select="Clvl" /></td>
          <td><xsl:value-of select="Cmoney" /></td>
        </tr>
        </xsl:for-each>
      </table>		
    </body>
  </html>		
</xsl:template>
</xsl:stylesheet>