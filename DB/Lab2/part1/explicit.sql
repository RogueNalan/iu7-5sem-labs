USE [dbGameServer]
GO

SELECT 1 AS Tag, NULL AS Parent,
	Cname AS [C!1!Cname],
	Clvl AS [C!1!Clvl],
	Cmoney AS [C!1!Cmoney]
FROM [C] AS Characters
FOR XML EXPLICIT
GO

SELECT 1 AS Tag, NULL AS Parent,
	Aname AS [A!1!Aname],
	Aphash AS [A!1!Aphash],
	AcrDate AS [A!1!ACrDate]
FROM [A] AS Accounts
FOR XML EXPLICIT
GO

SELECT 1 AS Tag, NULL AS Parent,
	Sname AS [S!1!Sname]
FROM [S] AS ServerNames
FOR XML EXPLICIT
GO

SELECT 
	Cname,
	Clvl
FROM [C]
WHERE Cno = 123 OR Cno = 456
FOR XML PATH ('TheChosenOne')
GO

USE [master]
GO 