use [dbGameServer]
go

select XMLval.exist('/A/Aphash')
from MyXML

select XMLval.value('/A[1]/Aname[1]', 'char(11)') as names
from MyXML

select XMLval.query('/A')
from MyXML

select Temp.Col.value('.', 'int')
from MyXML
cross apply XMLval.nodes('/A/Aphash') as Temp(Col)
go

use [master]
go