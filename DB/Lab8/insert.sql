use [dbGameServer]
go

truncate table MyXml

insert into MyXML (XMLval) values 
('<A>
  <Ano>1</Ano>
  <Aname>Ojohxhnlmmfzt   </Aname>
  <Aphash>872121466</Aphash>
  <AcrDate>1900-01-01</AcrDate>
</A>')

insert into MyXML (XMLval) values
('<A>
  <Ano>2</Ano>
  <Aname>Hblrtlukyidi    </Aname>
  <Aphash>790062739</Aphash>
  <AcrDate>1900-01-01</AcrDate>
</A>')

insert into MyXML (XMLval) values
('<A>
  <Ano>3</Ano>
  <Aname>Hlinppylnydjb   </Aname>
  <Aphash>1907849166</Aphash>
  <AcrDate>1900-01-01</AcrDate>
</A>')

select * from MyXML
go