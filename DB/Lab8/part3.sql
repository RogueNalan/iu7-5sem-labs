use [dbGameServer]
go

select XMLval.query
('
	for $x in /A
	return
	(
		<Account id = "{sql:column("Mno")}" />
	)
') as Accounts
from MyXML

declare @var int
set @var = 10
select XMLval.query
('
	for $x in /A
	return
	(
		<Account testval = "{sql:variable("@var")}" />
	)
')
from MyXML

use [master]
go