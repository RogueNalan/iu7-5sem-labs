﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Lab10
{
    class Program
    {
        static void Main(string[] args)
        {
            Action[] acts = new Action[]
            {
                Task1, Task2, Task3, Task4, Task5, Task6, Task7, Task8, Task9, Task10
            };

            for (int i = 0; i < acts.Length; ++i)
            {
                Console.WriteLine(String.Format("Task {0}:", i + 1));
                acts[i]();
            }

            conn.Close();
            Console.Read();
        }

        static void Task1()
        {
            conn = new SqlConnection(connectionString);
            try
            {
                conn.Open();
                Console.Write("Connection Opened!");
            }
            catch (SqlException e)
            {
                Console.WriteLine("Error: " + e.Message);
                conn.Close();
                Console.WriteLine("Connection closed!");
            }
            Console.Read();
        }

        static void Task2()
        {
            string queryString = @"select count(*) from A";
            var cmd = new SqlCommand(queryString, conn);

            Console.WriteLine(String.Format("Account number: {0}", cmd.ExecuteScalar()));
            Console.Read();
        }

        static void Task3()
        {
            string queryString = @"select Sname from S order by Sno asc";
            var cmd = new SqlCommand(queryString, conn);

            var reader = cmd.ExecuteReader();
            Console.WriteLine("Server names:");

            while (reader.Read())
                Console.WriteLine(reader.GetValue(0));
            Console.Read();
            reader.Close();
        }

        static void Task4()
        {
            string queryIns = @"insert into S values ('abc', 5000)";
            string queryUpd = @"update S set Sname = 'cde' where Smaxplayers = 5000";
            string queryDel = @"delete from S where Sno > 10";

            Console.WriteLine("Before:");
            Task3();
            Console.Read();

            var comm = new SqlCommand(queryIns, conn);
            comm.ExecuteNonQuery();
            Console.WriteLine("After insert:");
            Task3();
            Console.Read();

            comm = new SqlCommand(queryUpd, conn);
            comm.ExecuteNonQuery();
            Console.WriteLine("After update:");
            Task3();
            Console.Read();

            comm = new SqlCommand(queryDel, conn);
            comm.ExecuteNonQuery();
            Console.WriteLine("After delete:");
            Task3();
            Console.Read();
        }


        static void Task5()
        {
            string queryString = @"update S set Sname = 'notupd' where Sno = 10";
            var comm = new SqlCommand(queryString, conn);
            comm.ExecuteNonQuery();

            comm = conn.CreateCommand();
            comm.CommandType = CommandType.StoredProcedure;
            comm.CommandText = "TestSql";

            Console.WriteLine("Before:");
            Task3();
            Console.Read();

            comm.ExecuteNonQuery();
            Console.WriteLine("After:");
            Task3();
            Console.Read();
        }

        static void Task6()
        {
            var comm = conn.CreateCommand();
            comm.CommandType = CommandType.StoredProcedure;
            comm.CommandText = "GetDoubleFact";

            var input = comm.Parameters.Add("@val", SqlDbType.Int);
            input.Direction = ParameterDirection.Input;
            input.Value = 5;

            var output = comm.Parameters.Add("@res", SqlDbType.Float);
            output.Direction = ParameterDirection.Output;

            comm.ExecuteNonQuery();

            Console.WriteLine(String.Format("Result: {0}", output.Value));
            Console.Read();
        }

        static void Task7()
        {
            string queryString = @"select * from S";

            var da = new SqlDataAdapter(queryString, conn);
            var ds = new DataSet();
            da.Fill(ds, "S");
            var dt = ds.Tables["S"];

            foreach (DataRow row in dt.Rows)
            {
                foreach (DataColumn col in dt.Columns)
                    Console.Write(row[col] + " ");
                Console.WriteLine();
            }

            Console.Read();

        }

        static void Task8()
        {
            string queryString = @"select * from S";

            string filter = @"Smaxplayers > 10000";
            string sort = @"Sno desc";

            var da = new SqlDataAdapter(queryString, conn);
            var ds = new DataSet();
            da.Fill(ds, "S");
            var dt = ds.Tables["S"];

            foreach (DataRow row in dt.Select(filter, sort))
            {
                foreach (DataColumn col in dt.Columns)
                    Console.Write(row[col] + " ");
                Console.WriteLine();
            }

            Console.Read();
        }

        static void Task9()
        {
            Console.WriteLine("Before:");
            Task3();

            string queryString = @"select * from S";
            string filter = @"Sno = 10";
            string queryUpd = @"update S set Sname = 'upd2' where Sno = 10";
            var comm = new SqlCommand(queryUpd, conn);

            var da = new SqlDataAdapter(queryString, conn);
            var ds = new DataSet();
            da.Fill(ds, "S");
            var dt = ds.Tables["S"];

            var res = dt.Select(filter);

            foreach (DataRow row in res)
                row["Sname"] = "upd2";

            da.UpdateCommand = comm;
            da.Update(ds, "S");
            Console.WriteLine("After: ");
            Task3();
            Console.ReadLine();
        }

        static void Task10()
        {
            string queryString = @"select * from S";
            var da = new SqlDataAdapter(queryString, conn);
            var ds = new DataSet();
            da.Fill(ds, "S");

            ds.WriteXml(@"out.xml");
        }

        static string connectionString = @"integrated security = true;initial catalog = dbGameServer;data source=localhost";
        static SqlConnection conn = null;
    }
}
