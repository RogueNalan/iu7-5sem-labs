#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main() 
{
pid_t pid;
printf("fork program starting\n");
pid = fork();
switch(pid) 
{
case -1:
perror("fork failed");
exit(1);
case 0:
pid = getpid();
char output[30];
sprintf (output, "Process %d started exec()", pid);
execlp ("echo", "echo", output, NULL);
fprintf (stderr, "Exec() error occured in process %d\n", pid);
break;
default:
pid = wait(NULL);
printf("Child process finished, PID: %d\n", pid);
break;
}
return 0;
}
