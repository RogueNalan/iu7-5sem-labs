#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

int main(void)
{
	char msg[32];
	int piped[2];
	pid_t pid;
	
	if (pipe (piped) == -1)
	{
		fprintf (stderr, "Pipe couldn't be created in process %d, exiting...\n", getpid());
		return -1;
	}
	
	pid = fork();
	
	switch (pid)
	{
		case -1:
			fprintf (stderr, "Fork failed in process %d, exiting...\n", getpid());
			return -2;
		case 0:
			pid = getpid();

			while (getppid() != 1)
			{
				printf ("Child process with PID %d and PPID %d is ready to process another string!\n", pid, getppid());
				close (piped[1]);
				char c;
				while (read (piped[0], &c, 1), c != '\n')
					putc (c, stdout);
				putc ('\n', stdout);
			}
			
			printf ("Child process with PID %d exited!\n", pid);
			break;
		default:
			while (1)
			{
				printf ("Type string to transmit or type \"Exit\" to exit\n");
				scanf ("%30s", msg);
				
				if (strcmp (msg, "Exit") == 0)
				{
					msg[0] = '\n'; msg[1] = '\0';
					close (piped[0]);
					write (piped[1], msg, 1);

					printf ("Parent process with PID %d exited!\n", getpid());
					break;
				}
				
				int len = strlen(msg);
				msg[len++] = '\n';
				msg[len] = '\0';
				
				close (piped[0]);
				write (piped[1], msg, len);
			}
	}
	
	return 0;
}
