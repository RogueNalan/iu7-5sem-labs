Мануал по выполнению второй лабы Рязановой:

ps
Флаги процесса отображаются в самом левом столбце и представляют собой 16-чную сумму значений флагов
Возможные флаги:

P_ADVLOCK      0x00001        Process may hold a POSIX advisory lock 
P_CONTROLT      0x00002      Has a controlling terminal 
P_KTHREAD      0x00004       Kernel thread 
P_NOLOAD      0x00008        Ignore during load avg calculations 
P_PPWAIT      0x00010        Parent is waiting for child to exec/exit 
P_PROFIL      0x00020        Has started profiling 
P_STOPPROF      0x00040      Has thread in requesting to stop prof 
P_SUGID      0x00100         Had set id privileges since last exec 
P_SYSTEM      0x00200        System proc: no sigs, stats or swapping 
P_SINGLE_EXIT      0x00400   Threads suspending should exit, not wait 
P_TRACED      0x00800        Debugged process being traced 
P_WAITED      0x01000        Someone is waiting for us 
P_WEXIT      0x02000         Working on exiting 
P_EXEC      0x04000          Process called exec 
P_SA      0x08000            Using scheduler activations 
P_CONTINUED      0x10000     Proc has continued from a stopped state 
P_STOPPED_SIG      0x20000   Stopped due to SIGSTOP/SIGTSTP 
P_STOPPED_TRACE      0x40000 Stopped because of tracing 
P_STOPPED_SINGLE      0x80000        Only one thread can continue 
P_PROTECTED      0x100000    Do not kill on memory overcommit 
P_SIGEVENT      0x200000     Process pending signals changed 
P_JAILED      0x1000000      Process is in jail 
P_INEXEC      0x4000000      Process is in execve()

Статусы процесса:
R (Running) - выполняется
D - непрерываемый сон (например, ждёт ввода)
T (Terminated) - убит
Z (Zombie) - убит, но породивший процесс его не прикрыл
S (Sleep) - спит // у Рязановой в методичке описаны статусы S и I, я у себя в Дебиане натыкался только на S и Sl