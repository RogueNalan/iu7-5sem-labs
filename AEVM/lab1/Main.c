                  
#include <LPC24xx.H>                  

#define pp1 1<<26
#define pp2 1<<27
#define pp3 1<<28
#define pp4 1<<29

void delay(void) {

	unsigned int i;
	for (i=0;i<0xfffff;i++){}
}

int main (void) {
  unsigned int n = 0;
  unsigned int s = 1;//not pressed
	   
  PINSEL0 	= 0;
	PINSEL1 	= 0;
	PINSEL2 	= 0;
	PINSEL3 	= 0;

  IODIR1 = pp2 | pp3 | pp4;

  IOSET1 =  pp2 | pp3 | pp4;


  while (1) {       
    if (!(IOPIN1 & pp1)) {
		if (s){
			s = 0;
			if(n>=5){  //35	
				int i;
				IOCLR1 = pp4;
				delay();
				IOSET1 = pp4;
				
				IOCLR1 = pp3;
				for (i=0; i< 6; i++) //36
					delay();
				IOSET1 = pp3;
				n = 0;	
			}else{
				IOCLR1 = pp4;
				delay();
				IOSET1 = pp4;
    		IOCLR1 = pp2;
				delay();
				IOSET1 = pp2;
				n++;
			}	
		}
    }
	else {
    	s = 1;
	}
  }
}
