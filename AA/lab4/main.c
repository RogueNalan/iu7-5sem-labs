#include <stdio.h>
#include <stdlib.h>

#include <windows.h>

#include "stringgener.h"
#include "searchfuncs.h"

static unsigned long long tick(void)
{
    volatile unsigned long long d;
    __asm__ __volatile__
    (
        "rdtsc":"=A"(d)
    );

    return d;
}

typedef struct
{
    int hLen;
    int nLen;
    HaystackType hType;
    int symbNum;
} GenerData;

typedef struct
{
    double time;
    double count;
} SearchStats;

static SearchStats GetStats (SearchFunction Search, const GenerData* gData, int times)
{
    SearchStats res;
    res.time  = -1;
    res.count = -1;

    if (times <= 0)
        return res;

    char haystack[gData->hLen + 1];
    char needle[gData->nLen + 1];

    SearchStrings currStrings;
    currStrings.haystack = haystack;
    currStrings.needle = needle;

    for (int i = 0; i < times; ++i)
    {
        GenerateStrings(gData->hLen, gData->nLen, gData->hType, gData->symbNum, &currStrings);
        SearchResult currResult;

        unsigned long long start = tick();
        currResult = Search (currStrings.haystack, currStrings.needle);
        res.time += tick() - start;
        res.count += currResult.compCount;
    }

    res.time  /= times;
    res.count /= times;

    return res;
}

static inline void CalcForAll(const GenerData* gData, int times)
{
    SearchStats res = GetStats(ClassicSearch, gData, times);
    printf ("Classic search: %lf %lf\n", res.time, res.count);

    res = GetStats(KMPSearch, gData, times);
    printf ("KMP search: %lf %lf\n", res.time, res.count);

    res = GetStats(BMSearch, gData, times);
    printf("BMSearch: %lf %lf\n\n", res.time, res.count);
}

static void Compute (int min, int max, int step, double coef, int times)
{
    for (int i = min; i <= max; i += step)
    {
        printf ("Length = %d\n", i);

        int nLen = i * coef;

        GenerData data;
        data.hLen = i;
        data.nLen = nLen;
        data.symbNum = 0;

        data.hType = hsNoNeedle;
        printf ("No needle:\n");
        CalcForAll(&data, times);

        data.hType = hsAtStart;
        printf("Needle at start:\n");
        CalcForAll(&data, times);

        data.hType = hsAtEnd;
        printf("Needle at end:\n");
        CalcForAll(&data, times);

        data.hType = hsSymbFail;
        data.symbNum = 0;
        printf ("First symbol fail:\n");
        CalcForAll(&data, times);

        data.symbNum = 1;
        printf("Second symbol fail:\n");
        CalcForAll(&data, times);

        data.symbNum = nLen - 1;
        printf("Last symbol fail:\n");
        CalcForAll(&data, times);

        data.symbNum = nLen - 2;
        printf("nLen - 2 symbol fail:\n");
        CalcForAll(&data, times);

        data.hType = hsRandom;
        data.symbNum = 0;
        printf("Random data:\n");
        CalcForAll(&data, times);

        printf("\n\n\n");
    }

    printf("Special case 1:\n");
    GenerData data;
    data.hLen = 7;
    data.nLen = 5;
    data.hType = hsSpecialCase1;
    data.symbNum = 0;
    CalcForAll(&data, times);

    printf("Special case 2:\n");
    data.hType = hsSpecialCase2;
    CalcForAll(&data, times);
}

int main(void)
{
    int minLen, maxLen, step;
    double coef;
    int times;

    HANDLE procHandle = GetCurrentProcess();
    SetPriorityClass(procHandle, REALTIME_PRIORITY_CLASS);

    printf("Input min length, max length, step, needle length coef and times to test:\n");
    scanf("%d %d %d %lf %d", &minLen, &maxLen, &step, &coef, &times);
    printf("\n");

    Compute(minLen, maxLen, step, coef, times);

    return 0;
}
