#ifndef STRINGGENER_H_INCLUDED
#define STRINGGENER_H_INCLUDED

typedef enum { hsNoNeedle, hsRandom, hsAtStart, hsAtEnd, hsSymbFail, hsSpecialCase1, hsSpecialCase2 } HaystackType;

typedef struct
{
    char* haystack;
    char* needle;
} SearchStrings;

void GenerateStrings (int hLen, int nLen, HaystackType hType, int symbNum, SearchStrings* buf);

#endif // STRINGGENER_H_INCLUDED
