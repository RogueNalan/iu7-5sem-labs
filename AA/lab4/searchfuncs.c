#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include "searchfuncs.h"

SearchResult ClassicSearch(const char* haystack, const char* needle)
{
    SearchResult res;
    res.pos = -1;
    res.compCount = 0;

    if ((!haystack) || (!needle))
        return res;

    int nLen = strlen(needle);

    if (nLen <= 0)      // Checking hLen and diff is not necessary
        return res;

    int diff = strlen(haystack) - nLen;

    for (int i = 0; i <= diff; ++i, ++haystack)
    {
        char changed = 0;
        for (int j = 0; j < nLen; ++j)
            if (++res.compCount, haystack[j] != needle[j])
            {
                changed = 1;
                break;
            }
        if (!changed)
        {
            res.pos = i;
            return res;
        }
    }

    return res;
}

static inline int ComputePrefixFunction(const char* str, int len, int* res)
{
    int val = 0;
    res[0] = val;

    int compCount = 0;
    for (int i = 1; i < len; ++i)
    {
        while ((val > 0) && (++compCount, str[i] != str[val]))
            val = res[val - 1];

        if (++compCount, str[i] == str[val])
            ++val;
        res[i] = val;
    }

    return compCount;
}

SearchResult KMPSearch(const char* haystack, const char* needle)
{
    SearchResult res;
    res.pos = -1;
    res.compCount = 0;

    if ((!haystack) || (!needle))
        return res;

    int nLen = strlen(needle);

    if (nLen <= 0)
        return res;

    int coefs[nLen];

    res.compCount += ComputePrefixFunction(needle, nLen, coefs);

    int pos = 0;
    int count = 0;
    while (*haystack != '\0')
    {
        while ((pos > 0) && (++res.compCount, *haystack != needle[pos]))
            pos = coefs[pos - 1];
        if (++res.compCount, *haystack == needle[pos])
            ++pos;
        if (pos == nLen)
        {
            res.pos = count - pos + 1;
            return res;
        }

        ++haystack;
        ++count;
    }

    return res;
}

static inline void InitPrefixFunction(int* arr, int size)
{
    for (int i = 0; i < size; ++i, ++arr)
        if (!*arr)
            ++(*arr);
}

static inline int max(int a, int b) { return (a > b) ? a : b; }

SearchResult BMSearch(const char* haystack, const char* needle)
{
    SearchResult res;
    res.pos = -1;
    res.compCount = 0;

    if ((!haystack) || (!needle))
        return res;

    int nLen = strlen(needle);

    static int stopSymbol[UCHAR_MAX + 1];
    static const size_t tableSize = (UCHAR_MAX + 1) * sizeof(int);

    memset(stopSymbol, -1, tableSize);

    int lim = nLen - 1;
    for (int i = 0; i < lim; ++i)
        stopSymbol[needle[i]] = i;

    int coefs[nLen];
    res.compCount += ComputePrefixFunction(needle, nLen, coefs);
    InitPrefixFunction(coefs, nLen);

    int diff = strlen(haystack) - nLen;

    int hPos = 0;
    while (hPos <= diff)
    {
        int nPos = nLen - 1;
        while (++res.compCount, haystack[nPos] == needle[nPos])
        {
            if (!nPos--)
            {
                res.pos = hPos;
                return res;
            }
        }

        int move = max(coefs[nPos], nLen - stopSymbol[haystack[nPos]]);
        haystack += move;
        hPos += move;
    }

    return res;
}
