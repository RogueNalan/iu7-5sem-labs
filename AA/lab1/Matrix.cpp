#include "Matrix.h"

#include <stdexcept>
#include <random>

class RandomSingleton
// Internal singleton for random number generation
{
    public:
        static std::default_random_engine& Get()
        {
            if (!value)
            {
                value = new std::default_random_engine;
                value->seed (time (0));
            }
            return *value;
        }
    private:
        static std::default_random_engine* value;
};

std::default_random_engine* RandomSingleton::value = 0;

Matrix::Matrix (int m, int n): values (0), m (0), n (0)
{
    //ctor
    AllocMatr (m, n);
}

Matrix::~Matrix()
{
    //dtor
    FreeMatr();
}

Matrix::Matrix(const Matrix& other): values(0), m(0), n(0)
{
    //copy ctor
    AllocMatr (other.m, other.n);
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            values[i][j] = other.values[i][j];
}

Matrix& Matrix::operator=(const Matrix& rhs)
{
    if (this == &rhs) return *this; // handle self assignment

    if ((m != rhs.m) || (n != rhs.n))
    {
        FreeMatr();
        AllocMatr(rhs.m, rhs.n);

        for (int i = 0; i < m; ++i)
            for (int j = 0; j < n; ++j)
                values[i][j] = rhs.values[i][j];
    }

    return *this;
}

double& Matrix::operator() (int i, int j) const
{
    if ((i < 0) || (i >= m) || (j < 0) || (j >= n))
        throw std::out_of_range ("No such element in matrix!");
    else
        return values[i][j];
}

void Matrix::Fill(double min, double max)
{
    using namespace std;

    static const double eps = 1e-7;

    if ((max - min) < eps)
        throw logic_error("Wrong min and max parameters!");

    uniform_real_distribution<double> distribution (min, max);
    default_random_engine& generator = RandomSingleton::Get();

    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            values[i][j] = distribution (generator);
}

void Matrix::Print(SettableWxGrid& grid) const
{
    grid.SetRows (m);
    grid.SetCols (n);

    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            grid.SetCellValue (wxString::Format ("%lf", values[i][j]), i, j);
}

void Matrix::AllocMatr(int m, int n)
{
    if ((m > 0) && (n > 0))
    {
        values = new double*[m];
        for (int i = 0; i < m; ++i)
            values[i] = new double[n];

        this->m = m;
        this->n = n;
    }
}

void Matrix::FreeMatr()
{
    for (int i = 0; i < m; ++i)
        delete[] values[i];
    delete[] values;
    values = 0;
}
