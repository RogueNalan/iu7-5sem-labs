#ifndef MULTMAX_H
#define MULTMAX_H

#include "Matrix.h"

class MultMatrix : public Matrix
{
    public:
        MultMatrix (int m, int n): Matrix (m ,n) {}

        friend MultMatrix UsualMult (const MultMatrix& a, const MultMatrix& b);
        friend MultMatrix Vinogradov (const MultMatrix& a, const MultMatrix& b);
        friend MultMatrix UpgradedVinogradov (const MultMatrix& a, const MultMatrix& b);
};

#endif // MULTMAX_H
