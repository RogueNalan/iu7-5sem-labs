/***************************************************************
 * Name:      lab1Main.cpp
 * Purpose:   Code for Application Frame
 * Author:     ()
 * Created:   2014-09-17
 * Copyright:  ()
 * License:
 **************************************************************/

#include "lab1Main.h"
#include <wx/msgdlg.h>

#include "../ticktimer.h"

//(*InternalHeaders(lab1Frame)
#include <wx/intl.h>
#include <wx/string.h>
//*)

//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}

//(*IdInit(lab1Frame)
const long lab1Frame::ID_STATICTEXT1 = wxNewId();
const long lab1Frame::ID_TEXTCTRL1 = wxNewId();
const long lab1Frame::ID_STATICTEXT2 = wxNewId();
const long lab1Frame::ID_TEXTCTRL2 = wxNewId();
const long lab1Frame::ID_BUTTON1 = wxNewId();
const long lab1Frame::ID_BUTTON2 = wxNewId();
const long lab1Frame::ID_GRID1 = wxNewId();
const long lab1Frame::ID_CHOICE1 = wxNewId();
const long lab1Frame::ID_BUTTON3 = wxNewId();
const long lab1Frame::ID_STATICTEXT3 = wxNewId();
const long lab1Frame::ID_STATICTEXT4 = wxNewId();
const long lab1Frame::ID_CHOICE2 = wxNewId();
const long lab1Frame::ID_BUTTON4 = wxNewId();
const long lab1Frame::ID_PANEL1 = wxNewId();
//*)

BEGIN_EVENT_TABLE(lab1Frame,wxFrame)
    //(*EventTable(lab1Frame)
    //*)
END_EVENT_TABLE()

lab1Frame::lab1Frame(wxWindow* parent,wxWindowID id)
{
    //(*Initialize(lab1Frame)
    wxBoxSizer* BoxSizer4;
    wxBoxSizer* BoxSizer6;
    wxBoxSizer* BoxSizer5;
    wxBoxSizer* BoxSizer7;
    wxBoxSizer* BoxSizer8;
    wxBoxSizer* BoxSizer2;
    wxBoxSizer* BoxSizer1;
    wxBoxSizer* BoxSizer9;
    wxBoxSizer* BoxSizer3;

    Create(parent, wxID_ANY, _("Lab1"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE, _T("wxID_ANY"));
    BoxSizer1 = new wxBoxSizer(wxHORIZONTAL);
    Panel1 = new wxPanel(this, ID_PANEL1, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, _T("ID_PANEL1"));
    BoxSizer2 = new wxBoxSizer(wxVERTICAL);
    BoxSizer3 = new wxBoxSizer(wxHORIZONTAL);
    StaticText1 = new wxStaticText(Panel1, ID_STATICTEXT1, _("��������:"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT1"));
    BoxSizer3->Add(StaticText1, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    tcCols = new wxTextCtrl(Panel1, ID_TEXTCTRL1, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL1"));
    BoxSizer3->Add(tcCols, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText2 = new wxStaticText(Panel1, ID_STATICTEXT2, _("�����:"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT2"));
    BoxSizer3->Add(StaticText2, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    tcRows = new wxTextCtrl(Panel1, ID_TEXTCTRL2, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL2"));
    BoxSizer3->Add(tcRows, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BoxSizer2->Add(BoxSizer3, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BoxSizer4 = new wxBoxSizer(wxHORIZONTAL);
    btnMakeMatrix1 = new wxButton(Panel1, ID_BUTTON1, _("������������� ������� 1"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON1"));
    BoxSizer4->Add(btnMakeMatrix1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnMake = new wxButton(Panel1, ID_BUTTON2, _("������������� ������� 2"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON2"));
    BoxSizer4->Add(btnMake, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BoxSizer2->Add(BoxSizer4, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BoxSizer5 = new wxBoxSizer(wxHORIZONTAL);
    swgMatrix = new SettableWxGrid(Panel1, ID_GRID1, wxDefaultPosition, wxDefaultSize, 0, _T("ID_GRID1"));
    swgMatrix->CreateGrid(5,5);
    swgMatrix->EnableEditing(false);
    swgMatrix->EnableGridLines(true);
    swgMatrix->SetDefaultCellFont( swgMatrix->GetFont() );
    swgMatrix->SetDefaultCellTextColour( swgMatrix->GetForegroundColour() );
    BoxSizer5->Add(swgMatrix, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BoxSizer2->Add(BoxSizer5, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BoxSizer6 = new wxBoxSizer(wxVERTICAL);
    BoxSizer7 = new wxBoxSizer(wxHORIZONTAL);
    chMult = new wxChoice(Panel1, ID_CHOICE1, wxDefaultPosition, wxDefaultSize, 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE1"));
    chMult->SetSelection( chMult->Append(_("����������� �����")) );
    chMult->Append(_("����� ���������"));
    chMult->Append(_("���������� ����� ���������"));
    BoxSizer7->Add(chMult, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnMult = new wxButton(Panel1, ID_BUTTON3, _("��������"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON3"));
    BoxSizer7->Add(btnMult, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BoxSizer6->Add(BoxSizer7, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BoxSizer9 = new wxBoxSizer(wxHORIZONTAL);
    StaticText3 = new wxStaticText(Panel1, ID_STATICTEXT3, _("����� ���������� ��������� � ������:"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT3"));
    BoxSizer9->Add(StaticText3, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    stTime = new wxStaticText(Panel1, ID_STATICTEXT4, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT4"));
    BoxSizer9->Add(stTime, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BoxSizer6->Add(BoxSizer9, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BoxSizer8 = new wxBoxSizer(wxHORIZONTAL);
    chOut = new wxChoice(Panel1, ID_CHOICE2, wxDefaultPosition, wxDefaultSize, 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE2"));
    chOut->SetSelection( chOut->Append(_("������� 1")) );
    chOut->Append(_("������� 2"));
    chOut->Append(_("������������"));
    BoxSizer8->Add(chOut, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnOut = new wxButton(Panel1, ID_BUTTON4, _("�������"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON4"));
    BoxSizer8->Add(btnOut, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BoxSizer6->Add(BoxSizer8, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BoxSizer2->Add(BoxSizer6, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    Panel1->SetSizer(BoxSizer2);
    BoxSizer2->Fit(Panel1);
    BoxSizer2->SetSizeHints(Panel1);
    BoxSizer1->Add(Panel1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    SetSizer(BoxSizer1);
    BoxSizer1->Fit(this);
    BoxSizer1->SetSizeHints(this);

    Connect(ID_BUTTON1,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&lab1Frame::OnbtnMakeMatrix1Click);
    Connect(ID_BUTTON2,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&lab1Frame::OnbtnMakeClick);
    Connect(ID_BUTTON3,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&lab1Frame::OnbtnMultClick);
    Connect(ID_BUTTON4,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&lab1Frame::OnbtnOutClick);
    //*)
}

lab1Frame::~lab1Frame()
{
    //(*Destroy(lab1Frame)
    //*)
}

void lab1Frame::OnQuit(wxCommandEvent& event)
{
    Close();
}

void lab1Frame::OnAbout(wxCommandEvent& event)
{
    wxString msg = wxbuildinfo(long_f);
    wxMessageBox(msg, _("Welcome to..."));
}

void lab1Frame::OnbtnMakeMatrix1Click(wxCommandEvent& event)
{
    delete matr1;
    delete mult;
    mult = 0;

    int n = wxAtoi (tcCols->GetValue());
    int m = wxAtoi (tcRows->GetValue());

    matr1 = new MultMatrix (m, n);
    matr1->Fill(-1, 1);
}

void lab1Frame::OnbtnMakeClick(wxCommandEvent& event)
{
    delete matr2;
    delete mult;
    mult = 0;

    int n = wxAtoi (tcCols->GetValue());
    int m = wxAtoi (tcRows->GetValue());

    matr2 = new MultMatrix (m, n);
    matr2->Fill(-1, 1);
}



void lab1Frame::OnbtnMultClick(wxCommandEvent& event)
{
    unsigned long long ticks;
    static TickTimer timer;

    delete mult;

    if ((!matr1) || (!matr2))
    {
        wxMessageBox (_("�� ������� ���� ��� ��� �������!"));
        return;
    }

    switch (chMult->GetSelection())
    {
    case 0:
        timer.Restart();
        mult = new MultMatrix (UsualMult(*matr1, *matr2));
        timer.Stop();
        ticks = timer.GetTicks();
        break;
    case 1:
        timer.Restart();
        mult = new MultMatrix (Vinogradov(*matr1, *matr2));
        timer.Stop();
        ticks = timer.GetTicks();
        break;
    case 2:
        timer.Restart();
        mult = new MultMatrix (UpgradedVinogradov(*matr1, *matr2));
        timer.Stop();
        ticks = timer.GetTicks();
        break;
    default:
        ticks = 0;
    }

    stTime->SetLabel (wxString::Format ("%lld", ticks));
}

void lab1Frame::OnbtnOutClick(wxCommandEvent& event)
{
    const MultMatrix* matr[] = {matr1, matr2, mult};
    int num = chOut->GetSelection();

    if (!matr[num])
    {
        wxMessageBox (_("������� �� �������!"));
        return;
    }

    matr[num]->Print (*swgMatrix);
}
