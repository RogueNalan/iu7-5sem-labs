#include "multmax.h"

MultMatrix UsualMult (const MultMatrix& a, const MultMatrix& b)
{
    MultMatrix res (0, 0);
    if (a.n == b.m)
        res.AllocMatr (a.m, b.n);

    for (int i = 0; i < res.m; ++i)
        for (int j = 0; j < res.n; ++j)
        {
            res.values[i][j] = 0;
            for (int k = 0; k < a.n; ++k)
                res.values[i][j] += a.values[i][k] * b.values[k][j];
        }

    return res;
}

MultMatrix Vinogradov (const MultMatrix& a, const MultMatrix& b)
{
    MultMatrix res (0, 0);
    if (a.n != b.m)
        return res;

    res.AllocMatr (a.m, b.n);

    double* aCoefs = new double[a.m];
    double* bCoefs = new double[b.n];

    for (int i = 0; i < a.m; ++i)
    {
        aCoefs[i] = 0;
        for (int j = 0; j < a.n / 2; ++j)
            aCoefs[i] = aCoefs[i] + a.values[i][2 * j] * a.values[i][2 * j + 1];
    }

    for (int i = 0; i < b.n; ++i)
    {
        bCoefs[i] = 0;
        for (int j = 0; j < b.m / 2; ++j)
            bCoefs[i] = bCoefs[i] + b.values[2 * j][i] * b.values[2 * j + 1][i];
    }

    for (int i = 0; i < a.m; ++i)
        for (int j = 0; j < b.n; ++j)
        {
            res.values[i][j] = -aCoefs[i] - bCoefs[j];
            for (int k = 0; k < a.n / 2; ++k)
                res.values[i][j] = res.values[i][j] + (a.values[i][2 * k] + b.values[2 * k + 1][j]) * (a.values[i][2 * k + 1] + b.values[2 * k][j]);
        }

    if ((a.n % 2) == 1)
    {
        for (int i = 0; i < a.m; ++i)
            for (int j = 0; j < b.n; ++j)
                res.values[i][j] = res.values[i][j] + a.values[i][a.n - 1] * b.values[b.m - 1][j];
    }

    delete[] aCoefs;
    delete[] bCoefs;

    return res;
}

MultMatrix UpgradedVinogradov (const MultMatrix& a, const MultMatrix& b)
{
    MultMatrix res (0, 0);
    if (a.n != b.m)
        return res;

    res.AllocMatr (a.m, b.n);

    double* aCoefs = new double[a.m];
    double* bCoefs = new double[b.n];

    for (int i = 0; i < a.m; ++i)
    {
        aCoefs[i] = 0;
        for (int j = 0; j < a.n - 1; j += 2)
            aCoefs[i] += a.values[i][j] * a.values[i][j + 1];
    }

    for (int i = 0; i < b.n; ++i)
    {
        bCoefs[i] = 0;
        for (int j = 0; j < b.m - 1; j += 2)
            bCoefs[i] += b.values[j][i] * b.values[j + 1][i];
    }

    bool isOdd = a.n & 1; // Same as (a.n % 2 == 1, but faster)

    for (int i = 0; i < a.m; ++i)
        for (int j = 0; j < b.n; ++j)
        {
            res.values[i][j] = -aCoefs[i] - bCoefs[j];
            for (int k = 0; k < a.n - 1; k += 2)
                res.values[i][j] += (a.values[i][k + 1] + b.values[k][j]) * (a.values[i][k] + b.values[k + 1][j]);
            if (isOdd)
                res.values[i][j] += a.values[i][a.n - 1] * b.values[b.m - 1][j];
        }

    delete[] aCoefs;
    delete[] bCoefs;

    return res;
}
