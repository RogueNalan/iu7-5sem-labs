#include "ticktimer.h"

#include <stdexcept>

extern "C" DLL_EXPORT BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
    switch (fdwReason)
    {
        case DLL_PROCESS_ATTACH:
            // attach to process
            // return FALSE to fail DLL load
            break;

        case DLL_PROCESS_DETACH:
            // detach from process
            break;

        case DLL_THREAD_ATTACH:
            // attach to thread
            break;

        case DLL_THREAD_DETACH:
            // detach from thread
            break;
    }
    return TRUE; // succesful
}

TickTimer::TickTimer(): startTick (0), stopTick (0), isStarted (false)
{
    //ctor
}

TickTimer::~TickTimer()
{
    //dtor
}

void TickTimer::Start()
{
    if (isStarted == true)
        throw std::logic_error("Timer was already started!");

    isStarted = true;
    startTick = Tick();
}

void TickTimer::Stop()
{
    if (isStarted == false)
        throw std::logic_error("Timer is not started!");

    isStarted = false;
    stopTick = Tick();
}

void TickTimer::Restart()
{
    isStarted = true;
    startTick = Tick();
}

unsigned long long TickTimer::GetTicks() const
{
    if ((isStarted == true) || (startTick == 0))
        throw std::logic_error("Can't get timer ticks!");

    return stopTick - startTick;
}

unsigned long long TickTimer::Tick() const
{
    unsigned long long d;

    __asm__ __volatile__ ("rdtsc" : "=A" (d) );

    return d;
}
